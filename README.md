# Ear Trainer

Ear Trainer is a mobile (android) application that helps to train a musician's ear. It uses a synthesizer to play a scale or chord and then has the user select via multiple choice (or played on a piano) what the chord was.