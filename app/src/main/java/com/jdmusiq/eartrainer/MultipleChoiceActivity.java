/* MultipleChoiceActivity.java - This file holds the MultipleChoiceActivity class.
 * This class is one of the gamemodes of the app, allowing the user to hear a
 * chord or a scale and be presented with different options to try to figure out
 * what it is.
 *
 * @author Jason Davis
 * @version 1.0
 */

package com.jdmusiq.eartrainer;

import android.animation.ArgbEvaluator;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MultipleChoiceActivity extends AppCompatActivity {

    // Global Preferences to hold once Activity is launched
    private int speed = 2;
    private int difficulty = 1;
    private boolean chords = true;
    private boolean scales = true;
    private boolean skipping = true;
    private boolean vibration = true;

    private PlayQuestion pq = null;

    private int playType = 0; // 0 = chord, 1 = scale
    private ChordAnswers chordAnswer;
    private ScaleAnswers scaleAnswer;
    private int answer = 0; // [1,4]
    private int shift = 0; // the amount to shift the midi notes
    
    private long lastButtonHit = 0;

    private final int MAX_SHIFT_DOWN = 6;
    private final int MAX_SHIFT_UP = 6;
    private final int BUTTON_BUFFER = 750; /// only allow hitting buttons every .75 seconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_choice);

        // setup the Home/Back Button
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayShowHomeEnabled(true);
            bar.setDisplayHomeAsUpEnabled(true);
        }

        // Button Variables
        final Button playAgainButton = findViewById(R.id.playagain);
        final Button skipButton = findViewById(R.id.skip);
        final Button quitButton = findViewById(R.id.quit);
        final Button choice1 = findViewById(R.id.choice1);
        final Button choice2 = findViewById(R.id.choice2);
        final Button choice3 = findViewById(R.id.choice3);
        final Button choice4 = findViewById(R.id.choice4);

        playAgainButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!allowButton()) return;
                playagain();
            }
        });
        skipButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                skip();
            }
        });
        quitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                quit();
            }
        });

        // Choice OnClickListener's
        choice1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                choice_selected(1);
            }
        });
        choice2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                choice_selected(2);
            }
        });
        choice3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                choice_selected(3);
            }
        });
        choice4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                choice_selected(4);
            }
        });

        // ensure the Correct Layout portion is "gone"
        View v = findViewById(R.id.correct_answer_layout);
        v.setVisibility(View.GONE);

        // pull preferences into variables
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        // Pull the preference variables
        try {
            this.speed = Integer.parseInt(sharedPref.getString("pref_speed", "2"));
            this.difficulty = Integer.parseInt(sharedPref.getString("pref_difficulty", "1"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
        this.chords = sharedPref.getBoolean("pref_chords", true);
        this.scales = sharedPref.getBoolean("pref_scales", true);
        this.vibration = sharedPref.getBoolean("pref_vibrate", true);

        // make sure both chords and scales aren't both false
        if (!this.chords && !this.scales) {
            this.chords = true;
            this.scales = true;
        }

        // Handle showing the Skip button
        this.skipping = sharedPref.getBoolean("pref_skipping", true);
        ViewGroup.LayoutParams params = playAgainButton.getLayoutParams();
        if (this.skipping) {
            // Show the button - adjust width
            skipButton.setVisibility(View.VISIBLE);
            params.width = convertDpToPx(100);
        } else {
            // Hide the button - adjust width
            skipButton.setVisibility(View.GONE);
            params.width = convertDpToPx(150);
        }
        playAgainButton.setLayoutParams(params);
        skipButton.setLayoutParams(params);
        quitButton.setLayoutParams(params);

        // Initialize the instrument & SoundsThreads
        pq = new PlayQuestion(this, new Piano(getApplicationContext()), this.speed);
        pq.start_engine();
        pq.start_sound_player();

        // spawn the new question
        FirstRun();
        /*newQuestion();

        // wait for the sound system to be realized before we play the sound
        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                do {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } while (!pq.is_sound_player_started());
                playagain();
            }
        });
        myThread.start();*/
    }

    @Override
    protected void onResume() {
        pq.start_engine();
        pq.start_sound_player();
        super.onResume();
    }

    @Override
    protected void onPause() {
        pq.stop_sound_player();
        pq.stop_engine();
        super.onPause();
    }

    @Override
    public boolean onSupportNavigateUp() {
        quit();
        return true;
    }

    private int convertDpToPx(int dp) {
        return (int) (dp * getResources().getDisplayMetrics().density);
    }

    private void playagain() {
        switch (playType) {
            case 0:
                // chord
                pq.playChord(chordAnswer.getNotes());
                break;
            case 1:
                pq.playScale(scaleAnswer.getNotes());
                break;
            default:
                // some error occurred
                quit();
                break;
        }
    }

    private void skip() {
        newQuestion();
        playagain();
    }

    private void quit() {
        pq.stop();
        finish();
    }

    private void choice_selected(int choice) {

        // Vibration handler
        Vibrator v = (Vibrator) getApplicationContext().getSystemService(getApplicationContext().VIBRATOR_SERVICE);



        if (answer == choice) {
            if (this.vibration)
                if (v != null)
                    v.vibrate(250);
            RightAnswerSelected();
        } else {
            WrongAnswerShake(choice);
            if (this.vibration)
                if (v != null)
                    v.vibrate(75);
        }
    }

    private void newQuestion() {
        Random r = new Random();
        ChordAnswers c1,c2,c3,c4;
        ScaleAnswers s1,s2,s3,s4;

        final Button choice1 = findViewById(R.id.choice1);
        final Button choice2 = findViewById(R.id.choice2);
        final Button choice3 = findViewById(R.id.choice3);
        final Button choice4 = findViewById(R.id.choice4);
        final TextView nameThat = findViewById(R.id.namethat);

        // find the new playType: chord = 0, scale = 1
        if (scales && chords) {
            playType = r.nextInt(2); // returns random 1 or 0
        } else if (scales && !chords) {
            playType = 1;
        } else if (!scales && chords) {
            playType = 0;
        } else {
            // this shouldn't happen but if so, do random
            playType = r.nextInt(2); // returns random 1 or 0
        }

        // find the answer id
        answer = r.nextInt(4) + 1; // this will give an [1,4]

        // do the shift
        newShift();

        switch (playType) {
            case 0:
                // Chords
                c1 = getRandomChord();
                c2 = getRandomChord(c1);
                c3 = getRandomChord(c1,c2);
                c4 = getRandomChord(c1,c2,c3);

                choice1.setText(c1.toString());
                choice2.setText(c2.toString());
                choice3.setText(c3.toString());
                choice4.setText(c4.toString());

                switch (answer) {
                    case 1:
                        chordAnswer = c1;
                        break;
                    case 2:
                        chordAnswer = c2;
                        break;
                    case 3:
                        chordAnswer = c3;
                        break;
                    case 4:
                        chordAnswer = c4;
                        break;
                }

                // set the name that
                nameThat.setText(R.string.name_that_chord);

                shiftNotes(chordAnswer.getNotes());
                break;
            case 1:
                // Scales
                s1 = getRandomScale();
                s2 = getRandomScale(s1);
                s3 = getRandomScale(s1,s2);
                s4 = getRandomScale(s1,s2,s3);

                choice1.setText(s1.toString());
                choice2.setText(s2.toString());
                choice3.setText(s3.toString());
                choice4.setText(s4.toString());

                switch (answer) {
                    case 1:
                        scaleAnswer = s1;
                        break;
                    case 2:
                        scaleAnswer = s2;
                        break;
                    case 3:
                        scaleAnswer = s3;
                        break;
                    case 4:
                        scaleAnswer = s4;
                        break;
                }

                // set the name that
                nameThat.setText(R.string.name_that_scale);

                shiftNotes(scaleAnswer.getNotes());
                break;
            default:
                // some error occurred
                quit();
                break;
        }
    }

    // shiftNotes: Shifts the notes up or down by a random value
    private void newShift() {
        Random r = new Random();
        this.shift = r.nextInt(MAX_SHIFT_UP + MAX_SHIFT_DOWN) - MAX_SHIFT_DOWN;
    }

    private void shiftNotes(int[] notes) {
        for (int i = 0; i < notes.length; i++) {
            notes[i] += this.shift;
        }
    }

    // getRandomChord - retrieves a random chord that doesn't match the ones provided
    private ChordAnswers getRandomChord() {
        Random r = new Random();
        boolean another = true;
        ChordAnswers[] values;
        ChordAnswers tmp;

        values = ChordAnswers.values();
        int chordAnswersLength = values.length;
        do {
            tmp = values[r.nextInt(chordAnswersLength)];

            if (tmp.getDifficulty() <= this.difficulty) {
                another = false;
            }
        } while (another);

        return tmp;
    }

    private ChordAnswers getRandomChord(ChordAnswers c1) {
        Random r = new Random();
        boolean another = true;
        ChordAnswers[] values;
        ChordAnswers tmp;

        values = ChordAnswers.values();
        int chordAnswersLength = values.length;
        do {
            tmp = values[r.nextInt(chordAnswersLength)];

            if (tmp.getDifficulty() <= this.difficulty && c1 != tmp) {
                another = false;
            }
        } while (another);

        return tmp;
    }

    private ChordAnswers getRandomChord(ChordAnswers c1, ChordAnswers c2) {
        Random r = new Random();
        boolean another = true;
        ChordAnswers[] values;
        ChordAnswers tmp;

        values = ChordAnswers.values();
        int chordAnswersLength = values.length;
        do {
            tmp = values[r.nextInt(chordAnswersLength)];

            if (tmp.getDifficulty() <= this.difficulty && c1 != tmp && c2 != tmp) {
                another = false;
            }
        } while (another);

        return tmp;
    }

    private ChordAnswers getRandomChord(ChordAnswers c1, ChordAnswers c2, ChordAnswers c3) {
        Random r = new Random();
        boolean another = true;
        ChordAnswers[] values;
        ChordAnswers tmp;

        values = ChordAnswers.values();
        int chordAnswersLength = values.length;
        do {
            tmp = values[r.nextInt(chordAnswersLength)];

            if (tmp.getDifficulty() <= this.difficulty && c1 != tmp && c2 != tmp && c3 != tmp) {
                another = false;
            }
        } while (another);

        return tmp;
    }

    // getRandomScale - retrieves a random scale that doesn't match the ones provided
    private ScaleAnswers getRandomScale() {
        Random r = new Random();
        boolean another = true;
        ScaleAnswers[] values;
        ScaleAnswers tmp;

        values = ScaleAnswers.values();
        int scaleAnswersLength = values.length;
        do {
            tmp = values[r.nextInt(scaleAnswersLength)];

            if (tmp.getDifficulty() <= this.difficulty) {
                another = false;
            }
        } while (another);

        return tmp;
    }

    private ScaleAnswers getRandomScale(ScaleAnswers s1) {
        Random r = new Random();
        boolean another = true;
        ScaleAnswers[] values;
        ScaleAnswers tmp;

        values = ScaleAnswers.values();
        int scaleAnswersLength = values.length;
        do {
            tmp = values[r.nextInt(scaleAnswersLength)];

            if (tmp.getDifficulty() <= this.difficulty && s1 != tmp) {
                another = false;
            }
        } while (another);

        return tmp;
    }

    private ScaleAnswers getRandomScale(ScaleAnswers s1, ScaleAnswers s2) {
        Random r = new Random();
        boolean another = true;
        ScaleAnswers[] values;
        ScaleAnswers tmp;

        values = ScaleAnswers.values();
        int scaleAnswersLength = values.length;
        do {
            tmp = values[r.nextInt(scaleAnswersLength)];

            if (tmp.getDifficulty() <= this.difficulty && s1 != tmp && s2 != tmp) {
                another = false;
            }
        } while (another);

        return tmp;
    }

    private ScaleAnswers getRandomScale(ScaleAnswers s1, ScaleAnswers s2, ScaleAnswers s3) {
        Random r = new Random();
        boolean another = true;
        ScaleAnswers[] values;
        ScaleAnswers tmp;

        values = ScaleAnswers.values();
        int scaleAnswersLength = values.length;
        do {
            tmp = values[r.nextInt(scaleAnswersLength)];

            if (tmp.getDifficulty() <= this.difficulty && s1 != tmp && s2 != tmp && s3 != tmp) {
                another = false;
            }
        } while (another);

        return tmp;
    }
    
    private boolean allowButton() {
        if (BUTTON_BUFFER < SystemClock.elapsedRealtime() - lastButtonHit) {
            lastButtonHit = SystemClock.elapsedRealtime();
            return true;
        }
        return false;
    }

    private void FirstRun() {
        AlphaAnimation animator;
        View v;
        TextView tv;

        // newQuestion()
        newQuestion();

        v = findViewById(R.id.correct_answer_layout);
        v.setVisibility(View.VISIBLE);
        v.setClickable(true);

        // set the initial text
        tv = findViewById(R.id.correct);
        tv.setText(R.string.lets_begin);

        animator = new AlphaAnimation(1.0f, 0.0f);
        animator.setDuration(500);
        animator.setFillAfter(true);
        animator.setStartOffset(750);
        animator.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                View v;
                TextView tv;

                v = findViewById(R.id.correct_answer_layout);
                v.setClickable(false);

                // set correct text back
                tv = findViewById(R.id.correct);
                tv.setText(R.string.correct);

                playagain();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(animator);
    }

    private void RightAnswerSelected() {
        AlphaAnimation animator;
        View v;

        v = findViewById(R.id.correct_answer_layout);

        v.setVisibility(View.VISIBLE);
        v.setClickable(true);

        animator = new AlphaAnimation(0.0f, 1.0f);
        animator.setDuration(250);
        animator.setFillAfter(true);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                AlphaAnimation animator;
                View v;

                v = findViewById(R.id.correct_answer_layout);

                newQuestion();

                animator = new AlphaAnimation(1.0f, 0.0f);
                animator.setDuration(500);
                animator.setFillAfter(true);
                animator.setStartOffset(750);
                animator.setAnimationListener(new AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        View v;
                        v = findViewById(R.id.correct_answer_layout);
                        v.setClickable(false);
                        playagain();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                v.startAnimation(animator);


            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(animator);
    }

    @SuppressLint("ObjectAnimatorBinding")
    private void WrongAnswerShake(int choice) {
        ObjectAnimator shaker, colorAnimatorUp, colorAnimatorDown;
        View v;
        int colorFrom, colorTo;

        colorFrom = getResources().getColor(R.color.colorPrimary);
        colorTo = Color.RED;

        switch (choice) {
            case 1: v = findViewById(R.id.choice1); break;
            case 2: v = findViewById(R.id.choice2); break;
            case 3: v = findViewById(R.id.choice3); break;
            case 4: v = findViewById(R.id.choice4); break;
            default: return;
        }

        shaker = ObjectAnimator.ofFloat(v, "translationX", 0, -15, 15, -15, 15, -15, 15, 0);
        shaker.setDuration(150);

        colorAnimatorUp = ObjectAnimator.ofInt(v.getBackground(), "color", colorFrom, colorTo);
        colorAnimatorUp.setEvaluator(new ArgbEvaluator());
        colorAnimatorUp.setInterpolator(new DecelerateInterpolator());
        colorAnimatorUp.setDuration(150);

        colorAnimatorDown = ObjectAnimator.ofInt(v.getBackground(), "color", colorTo, colorFrom);
        colorAnimatorDown.setEvaluator(new ArgbEvaluator());
        colorAnimatorDown.setInterpolator(new DecelerateInterpolator());
        colorAnimatorDown.setDuration(500);

        AnimatorSet set = new AnimatorSet();
        set.play(shaker).with(colorAnimatorUp);
        set.play(colorAnimatorDown).after(colorAnimatorUp);
        set.start();
    }
}
