/* ChordAnswers.java - this file holds the ChordAnswers enum which holds
 * all of the different chords the app can play
 *
 * @author Jason Davis
 * @version 1.0
 * @see com.jdmusiq.eartrainer.ScaleAnswers
 */

package com.jdmusiq.eartrainer;

public enum ChordAnswers {
    M       (   "M",        "Major",                    1,  new int[] {48,52,55} ),
    m       (   "m",        "Minor",                    1,  new int[] {48,51,55} ),
    aug     (   "aug",      "Augmented",                1,  new int[] {48,52,56} ),
    dim     (   "dim",      "Diminished",               1,  new int[] {48,51,54} ),
    dim7    (   "dim7",     "Diminished seventh",       2,  new int[] {48,51,54,57} ),
    min7b5  (   "min7b5",   "Half-Diminished seventh",  3,  new int[] {48,51,54,58} ),
    min7    (   "min7",     "Minor Seventh",            2,  new int[] {48,51,55,58} ),
    mM7     (   "mM7",      "Minor Major Seventh",      3,  new int[] {48,51,55,59} ),
    dom7    (   "7",        "Dominant Seventh",         2,  new int[] {48,52,55,58} ),
    M7      (   "M7",       "Major Seventh",            2,  new int[] {48,52,55,59} ),
    aug7    (   "aug7",     "Augmented Seventh",        3,  new int[] {48,52,56,58} ),
    augM7   (   "augM7",    "Augmented Major seventh",  3,  new int[] {48,52,56,59} ),
    nine    (   "9",        "Dominant Ninth",           2,  new int[] {48,52,55,58,62} ),
    eleven  (   "11",       "Dominant Eleventh",        3,  new int[] {48,52,55,58,62,65} ),
    thirteen(   "13",       "Dominant Thirteenth",      3,  new int[] {48,52,55,58,62,65,69} ),
    m9      (   "m9",       "Minor Ninth",              2,  new int[] {48,51,55,58,62} ),
    m11     (   "m11",      "Minor Eleventh",           3,  new int[] {48,51,55,58,62,65} ),
    m13     (   "m13",      "Minor Thirteenth",         3,  new int[] {48,51,55,58,62,65,68} ),
    sus2    (   "sus2",     "Suspended Second",         2,  new int[] {48,50,55} ),
    sus4    (   "sus4",     "Suspended Fourth",         2,  new int[] {48,53,55} )
    ;

    private final String short_name;
    private final String long_name;
    private final int difficulty;
    private int[] notes;

    ChordAnswers(final String short_name, final String long_name, final int difficulty, int[] notes) {
        this.short_name = short_name;
        this.long_name = long_name;
        this.difficulty = difficulty;
        this.notes = notes;
    }

    @Override
    public String toString() {
        return long_name;
    }

    public String getShortName() {
        return this.short_name;
    }

    public String getLongName() {
        return this.long_name;
    }

    public int getDifficulty() {
        return this.difficulty;
    }

    public int[] getNotes() {
        return this.notes;
    }
}
