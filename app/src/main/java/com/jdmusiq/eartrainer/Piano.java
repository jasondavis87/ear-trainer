/* Piano.java - This class contains the Piano Class which holds the information to
 * play piano sounds using the mySampler Class. This class acts as a bridge between
 * the sounds, the sampler and the parent class that calls the notes.
 *
 * @author Jason Davis
 * @version 1.0
 * @see com.jdmusiq.eartrainer.InstrumentInterface
 */

package com.jdmusiq.eartrainer;

import android.content.Context;

public class Piano implements InstrumentInterface{
    private MySampler mySampler;
    final static int volume = 100;
    final static int velocity = 100;
    final static String DIRECTORY = "piano/";
    final static int UID = 10000;

    public Piano(Context context) {
        mySampler = new MySampler(context);
    }

    public void start_engine() {
        mySampler.start_engine();
    }

    public void stop_engine() {
        mySampler.stop_engine();
    }

    public void start_sound_player() {
        mySampler.start_sound_player();
    }

    public void stop_sound_player() {
        mySampler.stop_sound_player();
    }

    public boolean is_sound_player_started() {
        return mySampler.is_sound_player_started();
    }

    public void playNote(int note) {
        mySampler.play_note(note);
    }

    public void stopNote(int note) {
        mySampler.stop_note(note);
    }

    public void stopAllNotes() {
        mySampler.stop_all_notes();
    }

    public void load_samples() {
        // make sure that the samples arent already loaded:
        if (mySampler.loaded_UID(UID)) { return; }

        // load Piano Sounds
        String paths[] = new String[128];
        int freqs[] = new int[128];

        for (int i = 0; i < 128; i++) {
            if (i/12 >= 1 && i/12 <= 5) {
                switch (i%12) {
                    case 0: // c
                        paths[i] = DIRECTORY + "c" + i/12 + ".wav";
                        freqs[i] = 0;
                        break;
                    case 1: // c#
                        paths[i] = DIRECTORY + "e" + i/12 + ".wav";
                        freqs[i] = 8344;
                        break;
                    case 2: // d
                        paths[i] = DIRECTORY + "e" + i/12 + ".wav";
                        freqs[i] = 5400;
                        break;
                    case 3: // d#
                        paths[i] = DIRECTORY + "e" + i/12 + ".wav";
                        freqs[i] = 2622;
                        break;
                    case 4: // e
                        paths[i] = DIRECTORY + "e" + i/12 + ".wav";
                        freqs[i] = 0;
                        break;
                    case 5: // f
                        paths[i] = DIRECTORY + "g" + i/12 + ".wav";
                        freqs[i] = 5400;
                        break;
                    case 6: // f#
                        paths[i] = DIRECTORY + "g" + i/12 + ".wav";
                        freqs[i] = 2622;
                        break;
                    case 7: // g
                        paths[i] = DIRECTORY + "g" + i/12 + ".wav";
                        freqs[i] = 0;
                        break;
                    case 8: // g#
                        paths[i] = DIRECTORY + "c" + (i/12+1) + ".wav";
                        freqs[i] = 11463;
                        break;
                    case 9: // a
                        paths[i] = DIRECTORY + "c" + (i/12+1) + ".wav";
                        freqs[i] = 8344;
                        break;
                    case 10: // a#
                        paths[i] = DIRECTORY + "c" + (i/12+1) + ".wav";
                        freqs[i] = 5400;
                        break;
                    case 11: // b
                        paths[i] = DIRECTORY + "c" + (i/12+1) + ".wav";
                        freqs[i] = 2622;
                        break;
                }
            } else if (i/12 == 6) {
                switch (i%12) {
                    case 0: // c6
                        paths[i] = DIRECTORY + "c" + i/12 + ".wav";
                        freqs[i] = 0;
                        break;
                    default:
                        paths[i] = "";
                        freqs[i] = 0;
                }
            } else {
                paths[i] = "";
                freqs[i] = 0;
            }
        }
        mySampler.load_midi_samples(UID, paths, freqs);
    }
}
