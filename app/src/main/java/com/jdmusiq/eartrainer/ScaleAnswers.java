/* ScaleAnswers.java - this file holds the ScaleAnswers enum which holds
 * all of the different scales the app can play
 *
 * @author Jason Davis
 * @version 1.0
 * @see com.jdmusiq.eartrainer.ChordAnswers
 */

package com.jdmusiq.eartrainer;

public enum ScaleAnswers {
    major           ("Major",           1,  new int[] {48,50,52,53,55,57,59,60} ),
    ionian          ("Ionian",          2,  new int[] {48,50,52,53,55,57,59,60} ),
    dorian          ("Dorian",          2,  new int[] {48,50,51,53,55,57,58,60} ),
    phrygian        ("Phrygian",        2,  new int[] {48,49,51,53,55,56,58,60} ),
    lydian          ("Lydian",          2,  new int[] {48,50,52,54,55,57,59,60} ),
    mixolydian      ("Mixolydian",      2,  new int[] {48,50,52,53,55,57,58,60} ),
    minor           ("Minor",           1,  new int[] {48,50,51,53,55,56,58,60} ),
    aeolian         ("Aeolian",         2,  new int[] {48,50,51,53,55,56,58,60} ),
    locrian         ("Locrian",         2,  new int[] {48,49,51,53,54,56,58,60} ),
    wholetone       ("Whole Tone",      1,  new int[] {48,50,52,54,56,58,60} ),
    chromatic       ("Chromatic",       1,  new int[] {48,49,50,51,52,53,54,55,56,57,58,59,60} ),
    dominant        ("Dominant",        1,  new int[] {48,50,52,53,55,57,58,60} ),
    bebop           ("Bebop",           3,  new int[] {48,50,52,53,55,56,57,59,60} ),
    bebopdom        ("Bebop Dominant",  3,  new int[] {48,50,52,53,55,57,58,59,60} ),
    blues           ("Blues",           2,  new int[] {48,51,53,54,55,58,60} ),
    gypsy           ("Gypsy",           3,  new int[] {48,50,51,54,55,56,58,60} ),
    majorpentatonic ("Major Pentatonic",1,  new int[] {48,50,52,55,57,60} ),
    minorpentatonic ("Minor Pentatonic",1,  new int[] {48,51,53,55,58,60} ),
    doriansharp4    ("Dorian #4",       3,  new int[] {48,50,51,54,55,57,58,60} ),
    melodicminor    ("Melodic Minor",   3,  new int[] {48,50,51,53,55,57,59,60} ),
    harmonicminor   ("Harmonic Minor",  3,  new int[] {48,50,51,53,55,56,59,60} ),
    ;

    private final String name;
    private final int difficulty;
    private int[] notes;

    ScaleAnswers(final String name, final int difficulty, int[] notes) {
        this.name = name;
        this.difficulty = difficulty;
        this.notes = notes;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return this.name;
    }

    public int getDifficulty() {
        return this.difficulty;
    }

    public int[] getNotes() {
        return this.notes;
    }

}
