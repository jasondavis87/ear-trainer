/* EarTrainer.java - This file holds the EarTrainer Activity. This activity
 * holds the home screen for the main program which allows the user
 * to initiate the different game modes.
 *
 * @author Jason Davis
 * @version 1.0
 */

package com.jdmusiq.eartrainer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class EarTrainer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ear_trainer);

        // Initial set of preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        // Attach the setOnClickListener
        final Button beginButton = findViewById(R.id.begin);
        beginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                begin();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //handle item selection
        switch (item.getItemId()) {
            case R.id.settings:
                Intent intent = new Intent(this, PrefsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void begin() {
        Context context = getApplicationContext();
        //Toast.makeText(context, "BEGIN BUTTON PRESSED", Toast.LENGTH_SHORT).show();

        // pull preferences into variables
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        int playmode = 1;
        try {
            playmode = Integer.parseInt(sharedPref.getString("pref_playmode", "1"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }

        switch (playmode) {
            case 1:
                Intent intent = new Intent(this,MultipleChoiceActivity.class);
                startActivity(intent);
                break;
            case 2:
                // Future holder for Piano Style Training
                break;
            default:
                Toast.makeText(context, "AN ERROR OCCURRED", Toast.LENGTH_LONG).show();
                finish();
        }

    }
}