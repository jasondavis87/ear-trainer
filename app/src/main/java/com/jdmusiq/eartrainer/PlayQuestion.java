/* PlayQuestion.java - This file holds the PlayQuestion class which handles
 * playing either a scale or a chord using an instance of InstrumentInterface
 * through a worker thread.
 *
 * @author Jason Davis
 * @version 1.0
 */

package com.jdmusiq.eartrainer;

import android.content.Context;

public class PlayQuestion {
    private SoundThread thread = null;
    private InstrumentInterface instrument;
    private Context context;
    private int speed;

    private final int CHORD_SPEED_MULTIPLIER = 650;
    private final int SCALE_SPEED_MULTIPLIER = 150;

    public PlayQuestion(Context context, InstrumentInterface instrument, int speed) {
        this.context = context;
        this.instrument = instrument;
        this.speed = speed;
    }

    public void playChord(int [] notes) {
        if (thread != null) {
            thread.requestStop();
            try {
                thread.join(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        thread = new SoundThread(instrument, true, speed*CHORD_SPEED_MULTIPLIER, notes);
        thread.start();
    }

    public void playScale(int [] notes) {
        if (thread != null) {
            thread.requestStop();
            try {
                thread.join(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        thread = new SoundThread(instrument, false, speed*SCALE_SPEED_MULTIPLIER, notes);
        thread.start();
    }

    public void stop() {
        if (thread != null) {
            thread.requestStop();
            try {
                thread.join(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void start_engine() {
        instrument.start_engine();
    }

    public void stop_engine() {
        instrument.stop_engine();
    }

    public void start_sound_player() {
        instrument.start_sound_player();
    }

    public void stop_sound_player() {
        instrument.stop_sound_player();
    }

    public boolean is_sound_player_started() {
        return instrument.is_sound_player_started();
    }

    public void load_samples() {
        instrument.load_samples();
    }



    private class SoundThread extends Thread {

        InstrumentInterface instrument = null;
        private boolean allTogether;
        private int milliseconds;
        private int [] notes;
        private boolean stop = false;
        private final int STOP_CHECK_DELAY = 10;

        public SoundThread(InstrumentInterface instrument, boolean allTogether, int milliseconds, int [] notes ) {
            super();
            this.instrument = instrument;
            this.allTogether = allTogether;
            this.milliseconds = milliseconds;
            this.notes = notes;
        }

        public void run() {
            int elapsed = 0;
            if (instrument == null) return;

            if (allTogether) {
                for (int note : notes) {
                    instrument.playNote(note);
                }

                while (elapsed < milliseconds || stop) {
                    elapsed += STOP_CHECK_DELAY;
                    try {
                        sleep(milliseconds);} catch (InterruptedException e) {e.printStackTrace();}
                }
                instrument.stopAllNotes();
            } else {
                for (int note : notes) {
                    instrument.playNote(note);
                    try {
                        sleep(milliseconds);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    instrument.stopNote(note);
                    if (stop) break;
                    instrument.stopAllNotes();
                }
            }
        }

        public synchronized void requestStop() { stop = true; }
    }
}
