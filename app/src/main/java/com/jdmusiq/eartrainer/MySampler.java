/* MySampler.java - This class contains the MySampler class. This class interfaces with
 * the NDK code to give a Java based interface to connect with the NDK Audio Engine.
 *
 * @author Jason Davis
 * @version 1.0
 */

package com.jdmusiq.eartrainer;

import android.content.Context;
import android.content.res.AssetManager;

public class MySampler {
    Context context;
    private final int SAMPLE_RATE = 44100;
    private final int FRAMES_PER_BUFFER = 512;

    // Constructor
    public MySampler(Context context) {
        String s = testjni();
        this.context = context;
    }

    // startEngine(): starts the sound Engine
    public void start_engine() {
        if (!isEngineStarted()){
            startEngine();
        }
    }

    // stopEngine(): stops the sound Engine
    public void stop_engine() {
        stopEngine();
    }

    // startSoundPlayer(): starts the sound player
    public void start_sound_player () {
        if (!isSoundPlayerStarted()) {
            startSoundPlayer(SAMPLE_RATE, FRAMES_PER_BUFFER);
        }
    }

    // is_sound_player_started
    public boolean is_sound_player_started() {
        return isSoundPlayerStarted();
    }

    // stopSoundPlayer(): stops the sound player
    public void stop_sound_player() {
        stopSoundPlayer();
    }

    public boolean load_midi_samples(int uid, String [] paths,int[] freqs) {
        return loadMidiSamples(uid, context.getAssets(), paths, freqs);
    }

    public boolean loaded_UID(int uid) {
        return loadedUID() == uid;
    }

    public void play_note( int note ) {
        playNote(note);
    }

    public void stop_note( int note ) {
        stopNote(note);
    }

    public void stop_all_notes() {
        stopAllNotes();
    }

    /*-------------------------------------------------------------------*/
    static {
        System.loadLibrary("mySampler");
    }
    /*  Native methods, implemented in the cpp folder */
    private native String testjni();
    private static native void startEngine();
    private static native void stopEngine();
    private static native void startSoundPlayer(int sampleRate, int framesPerBuffer);
    private static native void stopSoundPlayer();
    private static native boolean isEngineStarted();
    private static native boolean isSoundPlayerStarted();
    private static native boolean isPlaying();
    private static native boolean loadMidiSamples(int UID, AssetManager assetManager, String[] paths, int[] freqs);
    private static native int loadedUID();
    private static native void playNote(int note);
    private static native void stopNote(int note);
    private static native void stopAllNotes();
}
