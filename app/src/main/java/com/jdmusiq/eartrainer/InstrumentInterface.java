/* InstrumentInterface.java - This file includes the InstrumentInterface interface.
 * The interface ensures each "Instrument" or sound subset includes the necessary
 * functions to play the notes in the sampler
 *
 * @author Jason Davis
 * @version 1.0
 */

package com.jdmusiq.eartrainer;

public interface InstrumentInterface {
    void playNote(int note);
    void stopNote(int note);
    void stopAllNotes();
    void start_engine();
    void stop_engine();
    void start_sound_player();
    void stop_sound_player();
    boolean is_sound_player_started();
    void load_samples();
}
