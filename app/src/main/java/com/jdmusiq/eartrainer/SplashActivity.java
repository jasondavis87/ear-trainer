/* SplashActivity.java - This class contains the SplashActivity Class which holds
 * the activity information for the Splash Screen thats used whilst the app is loading
 * it's sounds.
 *
 * @author Jason Davis
 * @version 1.0
 */

package com.jdmusiq.eartrainer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread LoadThread = new Thread() {
            @Override
            public void run() {
                Piano p = new Piano(getApplicationContext());
                p.load_samples();

                Intent intent = new Intent(getApplicationContext(), EarTrainer.class);
                startActivity(intent);
                finish();
            }
        };
        LoadThread.start();

    }
}
