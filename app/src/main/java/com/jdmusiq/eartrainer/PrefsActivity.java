/* SplashActivity.java - This class contains the PrefsActivity Class which displays the
 * preferences and allows the user to change the app settings
 *
 * @author Jason Davis
 * @version 1.0
 */

package com.jdmusiq.eartrainer;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PrefsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment()).commit();

        ActionBar bar = getSupportActionBar();
        if (bar != null ) {
            bar.setDisplayShowHomeEnabled(true);
            bar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    /**
     * LicenseDialogFragment - This class handles displaying the open_source_license.html file
     * for legal reasons.
     * @author Jason Davis
     * @version 1.0
     */
    public static class LicenseDialogFragment extends DialogFragment {
        public static LicenseDialogFragment newInstance() {
            return new LicenseDialogFragment();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            WebView view = (WebView) LayoutInflater.from(getActivity()).inflate(R.layout.dialog_licenses, null);
            view.setWebViewClient(new WebViewClient());
            view.loadUrl("file:///android_asset/open_source_licenses.html");
            return new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog_Alert)
                    .setTitle(getString(R.string.action_licenses))
                    .setView(view)
                    .setPositiveButton(android.R.string.ok, null)
                    .create();
        }
    }

    /**
     * PrefsFragment - This class handles the Preference Fragement
     *
     * @author Jason Davis
     * @version 1.0
     */
    public static class PrefsFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences);

            Preference pref_legal = findPreference("pref_licenses");
            pref_legal.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    LicenseDialogFragment dialog = LicenseDialogFragment.newInstance();
                    dialog.show(getActivity().getFragmentManager(), "licenses");
                    return false;
                }
            });
        }
    }
}