/**
 * mySampler.c - This File includes all of the NDK code for the
 * Java Class: MySampler, including it's private functions & subroutines.
 * The functionality includes, running OpenSLES implementation for the sound
 * engine as well as a sample based synthesizer that re-samples (stretch-tunes)
 * notes to another note to ensure all notes are covered with less data.
 *
 * @author Jason Davis
 * @version 1.0
 */

#include "mySampler.h"

JNIEXPORT jstring JNICALL
Java_com_jdmusiq_eartrainer_MySampler_testjni(JNIEnv *env, jobject instance) {
    // JNI Test function
    return (*env)->NewStringUTF(env, "Testing the JNI");
}

JNIEXPORT void JNICALL
Java_com_jdmusiq_eartrainer_MySampler_startEngine(JNIEnv *env, jclass type) {
    SLresult result;

    LOGI("Starting sound service");

    const SLuint32 engineMixIIDCount = 1;
    const SLInterfaceID engineMixIIDs[] = {SL_IID_ENGINE};
    const SLboolean engineMixReqs[] = {SL_BOOLEAN_TRUE};

    // create engine
    result = slCreateEngine(&mEngineObj, 0, NULL,
                            engineMixIIDCount, engineMixIIDs, engineMixReqs);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the engine
    result = (*mEngineObj)->Realize(mEngineObj, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the engine interface, which is needed in order to create other objects
    result = (*mEngineObj)->GetInterface(mEngineObj, SL_IID_ENGINE, &mEngine);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // Create the mixed output
    const SLuint32 outputMixIIDCount = 0;
    const SLInterfaceID outputMixIIDs[] = {};
    const SLboolean outputMixReqs[] = {};

    // create output mix
    result = (*mEngine)->CreateOutputMix(mEngine, &mOutputMixObj,
                                         outputMixIIDCount, outputMixIIDs, outputMixReqs);
    assert(SL_RESULT_SUCCESS == result);
    // realize the output mix
    result = (*mOutputMixObj)->Realize(mOutputMixObj, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);

    LOGI("Sound Service Started Successfully");
}

JNIEXPORT void JNICALL
Java_com_jdmusiq_eartrainer_MySampler_stopEngine(JNIEnv *env, jobject instance) {

    LOGI("Destroying Sound Service");

    // destroy output mix object, and invalidate all associated interfaces
    if (mOutputMixObj != NULL) {
        (*mOutputMixObj)->Destroy(mOutputMixObj);
        mOutputMixObj = NULL;
    }

    // destroy engine object, and invalidate all associated interfaces
    if (mEngineObj != NULL) {
        (*mEngineObj)->Destroy(mEngineObj);
        mEngineObj = NULL;
        mEngine = NULL;
    }

    LOGI("Sound service destroyed successfully");
}

JNIEXPORT void JNICALL
Java_com_jdmusiq_eartrainer_MySampler_startSoundPlayer(JNIEnv *env, jclass type, jint sampleRate, jint framesPerBuffer) {
    // check the incoming variables
    if (sampleRate <= 0) {
        mSampleRate = DEFAULT_SAMPLE_RATE;
    } else {
        mSampleRate = sampleRate;
    }

    if (framesPerBuffer <= 0) {
        mFramesPerBuffer = DEFAULT_FRAMES_PER_BUFFER;
    } else {
        mFramesPerBuffer = framesPerBuffer;
    }

    // clear sounds
    int i;
    for (i = 0; i < AUDIO_OUT_CHANNELS; i++) {
        mSounds[i].mUsed = false;
    }

    SLresult result;

    // INPUT
    // audio source with maximum of two buffers in the queue
    // where are data
    SLDataLocator_AndroidSimpleBufferQueue dataLocatorInput;
    dataLocatorInput.locatorType = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
    dataLocatorInput.numBuffers = 2;

    // format of data
    SLDataFormat_PCM dataFormat;
    dataFormat.formatType = SL_DATAFORMAT_PCM;
    dataFormat.numChannels = 1;
    dataFormat.samplesPerSec = (SLuint32) mSampleRate * 1000;
    dataFormat.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
    dataFormat.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;
    dataFormat.channelMask = SL_SPEAKER_FRONT_CENTER;
    dataFormat.endianness = SL_BYTEORDER_LITTLEENDIAN;

    SLDataSource dataSource;
    dataSource.pLocator = &dataLocatorInput;
    dataSource.pFormat = &dataFormat;

    SLDataLocator_OutputMix dataLocatorOut;
    dataLocatorOut.locatorType = SL_DATALOCATOR_OUTPUTMIX;
    dataLocatorOut.outputMix = mOutputMixObj;

    SLDataSink dataSink;
    dataSink.pLocator = &dataLocatorOut;
    dataSink.pFormat = NULL;

    // Create the sound player
    const SLuint32 soundPlayerIIDCount = 3;
    const SLInterfaceID soundPlayerIIDs[] = {SL_IID_PLAY, SL_IID_BUFFERQUEUE, SL_IID_VOLUME};
    const SLboolean soundPlayerReqs[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};

    result = (*mEngine)->CreateAudioPlayer(mEngine, &mSoundPlayerObj, &dataSource, &dataSink,
                                           soundPlayerIIDCount, soundPlayerIIDs, soundPlayerReqs);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // Realize the sound player
    result = (*mSoundPlayerObj)->Realize(mSoundPlayerObj, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // Get all the interfaces
    result =(*mSoundPlayerObj)->GetInterface(mSoundPlayerObj, SL_IID_PLAY, &mSoundPlayer);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    result =(*mSoundPlayerObj)->GetInterface(mSoundPlayerObj, SL_IID_BUFFERQUEUE, &mSoundQueue);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    result =(*mSoundPlayerObj)->GetInterface(mSoundPlayerObj, SL_IID_VOLUME, &mSoundVolume);
    assert(SL_RESULT_SUCCESS == result);
    (*mSoundVolume)->SetVolumeLevel(mSoundVolume, (SLmillibel) 0);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // Register the callback for the queue
    result = (*mSoundQueue)->RegisterCallback(mSoundQueue, soundPlayerCallback, NULL);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    mTmpSoundBuffer = (signed int *)malloc(sizeof(signed int) * mFramesPerBuffer * 4);
    mAudioOutSoundData1 = (signed short *)malloc(sizeof(signed short) * mFramesPerBuffer * 2);
    mAudioOutSoundData2 = (signed short *)malloc(sizeof(signed short) * mFramesPerBuffer * 2);

    memset(mAudioOutSoundData1, 0, sizeof(signed short) * mFramesPerBuffer);
    memset(mAudioOutSoundData2, 0, sizeof(signed short) * mFramesPerBuffer);

    mActiveAudioOutSoundBuffer = mAudioOutSoundData1;

    // send two buffers
    sendSoundBuffer();
    sendSoundBuffer();

    // Start Playing Silence
    result = (*mSoundPlayer)->SetPlayState(mSoundPlayer, SL_PLAYSTATE_PLAYING);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    LOGI("Created the Sound Player Successfully");
    LOGI("Running At %dHz with %d Channel(s)", sampleRate, dataFormat.numChannels);
}

JNIEXPORT void JNICALL
Java_com_jdmusiq_eartrainer_MySampler_stopSoundPlayer(JNIEnv *env, jclass type) {
    LOGI("Sound Player Stopping ...");
    if (mSoundPlayerObj != NULL) {
        SLuint32 soundPlayerState;
        (*mSoundPlayerObj)->GetState(mSoundPlayerObj, &soundPlayerState);
        if (soundPlayerState == SL_OBJECT_STATE_REALIZED) {
            (*mSoundQueue)->Clear(mSoundQueue);
            (*mSoundPlayerObj)->AbortAsyncOperation(mSoundPlayerObj);
            (*mSoundPlayerObj)->Destroy(mSoundPlayerObj);
            mSoundPlayerObj = NULL;
            mSoundPlayer = NULL;
            mSoundQueue = NULL;
            mSoundVolume = NULL;
            LOGI("Sound Player Stopped Successfully");
        }
    }

    // free the buffers
    free(mTmpSoundBuffer);
    free(mAudioOutSoundData1);
    free(mAudioOutSoundData2);
}




JNIEXPORT jboolean JNICALL Java_com_jdmusiq_eartrainer_MySampler_isEngineStarted(JNIEnv *env, jclass type) {
    SLuint32 pState;

    if (mOutputMixObj == NULL) {
        LOGI("isEngineStarted: mOutputMixObj is NULL");
        return JNI_FALSE;
    } else {
        if ( (*mOutputMixObj)->GetState(mOutputMixObj, &pState) == SL_RESULT_SUCCESS ) {
            if (pState == SL_OBJECT_STATE_REALIZED) {
                LOGI("isEngineStarted: TRUE");
                return JNI_TRUE;
            } else {
                LOGI("isEngineStarted: the state is not realized");
                return JNI_FALSE;
            }
        } else {
            LOGI("isEngineStarted: failed to get state");
            return JNI_FALSE;
        }
    }
}


JNIEXPORT jboolean JNICALL Java_com_jdmusiq_eartrainer_MySampler_isSoundPlayerStarted(JNIEnv *env, jclass type) {
    SLuint32 pState;

    if (mSoundPlayerObj == NULL) {
        LOGI("isSoundPlayerStarted: mSoundPlayerObj is NULL");
        return JNI_FALSE;
    } else {
        if ( (*mSoundPlayerObj)->GetState(mSoundPlayerObj, &pState) == SL_RESULT_SUCCESS ) {
            if (pState == SL_OBJECT_STATE_REALIZED) {
                LOGI("isSoundPlayerStarted: TRUE");
                return JNI_TRUE;
            } else {
                LOGI("isSoundPlayerStarted: the state is not realized");
                return JNI_FALSE;
            }
        } else {
            LOGI("isSoundPlayerStarted: failed to get state");
            return JNI_FALSE;
        }
    }
}

JNIEXPORT jboolean JNICALL Java_com_jdmusiq_eartrainer_MySampler_isPlaying(JNIEnv *env, jclass type) {
    SLuint32 pState;

    if (mSoundPlayerObj == NULL) {
        LOGI("isSoundPlayerStarted: mSoundPlayerObj is NULL");
        return JNI_FALSE;
    } else {
        if ( (*mSoundPlayerObj)->GetState(mSoundPlayerObj, &pState) == SL_RESULT_SUCCESS ) {
            if (pState == SL_OBJECT_STATE_REALIZED) {
                LOGI("isSoundPlayerStarted: TRUE");
                return JNI_TRUE;
            } else {
                LOGI("isSoundPlayerStarted: the state is not realized");
                return JNI_FALSE;
            }
        } else {
            LOGI("isSoundPlayerStarted: failed to get state");
            return JNI_FALSE;
        }
    }
}

JNIEXPORT jboolean JNICALL Java_com_jdmusiq_eartrainer_MySampler_loadMidiSamples(JNIEnv *env, jclass type, jint uid, jobject assetManager, jobjectArray pathStringArray, jintArray freqIntArray) {
    int i, j;
    jstring tmpString;
    char paths[128][1024]; // file path shouldnt be longer than 1024 characters
    SwrContext *resample_context = NULL;
    uint8_t **converted_input_samples = NULL;

    // error checking
    if ((*env)->GetArrayLength(env, pathStringArray) != 128) return JNI_FALSE; // it must include all 127 note values
    if ((*env)->GetArrayLength(env, freqIntArray) != 128) return JNI_FALSE; // it must include all 127 note values

    // get our variables
    memset(paths, 0, sizeof(char) * 128 * 128);
    for (i = 0; i < 128; i++) {
        // the Path String
        tmpString = (jstring) ((*env)->GetObjectArrayElement(env, pathStringArray, i));
        const char *rawString = (*env)->GetStringUTFChars(env, tmpString, 0);
        if (rawString != NULL) strcpy(paths[i], rawString);
        (*env)->ReleaseStringUTFChars(env, tmpString, rawString);
    }

    jint *freqs = (*env)->GetIntArrayElements(env, freqIntArray, false);

    // Do the work
    AAssetManager *mgr = AAssetManager_fromJava(env, assetManager);
    assert(mgr != NULL);

    for (i = 0; i < 128; i++) {
        if (strcmp(paths[i], "") != 0) {
            AAsset *asset = AAssetManager_open(mgr, paths[i], AASSET_MODE_UNKNOWN);
            if (asset == NULL) return JNI_FALSE; // couldn't open file

            off_t start, length;
            length = AAsset_getLength(asset);
            start = AAsset_seek(asset, WAVE_FILE_HEADER_SIZE, SEEK_SET);

            uint8_t *buffer = malloc(sizeof(char) * length); // 46 bit buffer
            length = AAsset_read(asset, buffer, length); // read too much but repalce the length var

            AAsset_close(asset);

            // pitch shift / resample
            if (freqs[i] != 0) {
                // convert the data to float [-1,1]
                /*float *float_buffer = malloc(sizeof(float) * length / 2);
                for (j = 0; j < length; j=j+2) {
                    short k = ((buffer[j+1] << 8) | buffer[j]);
                    float_buffer[j/2] = convertAudioShortToFloat(k);
                }*/

                if (init_resampler(&resample_context, DEFAULT_SAMPLE_RATE, DEFAULT_SAMPLE_RATE+freqs[i])) {
                    swr_free(&resample_context);
                    return JNI_FALSE;
                }

                init_converted_samples(&converted_input_samples, length);

                // ffmpeg expects the samples to be in a nested array of different channels
                // i.e. buffer[0][x] contains all the audio data for track 0 (mono)
                uint8_t **buffer2;
                buffer2 = calloc(1, sizeof(uint8_t*));
                buffer2[0] = buffer;


                if (convert_samples((const uint8_t **) buffer2, converted_input_samples, length, resample_context)) {
                    swr_free(&resample_context);
                    return JNI_FALSE;
                }

                // set the data
                memcpy(buffer, (converted_input_samples[0]), length * sizeof(uint8_t));

                // resampling cleanup
                if (converted_input_samples) {
                    if (converted_input_samples[0]) av_freep(&converted_input_samples[0]);
                    free(converted_input_samples);
                }

                buffer2[0] = NULL;
                free(buffer2);
                swr_free(&resample_context);


                // convert back to byte/char data and replace the buffer
                // no need to reallocate - the size should be the same
                //smbPitchShift(freqs[i]/(float)DEFAULT_SAMPLE_RATE, length/2, MAX_FRAME_LENGTH, 4, DEFAULT_SAMPLE_RATE, float_buffer, float_buffer);

                /*for (j = 0; j < length; j=j+2) {
                    short k = convertAudioFloatToShort(float_buffer[j/2]);
                    buffer[j] = k & 0xFF;
                    buffer[j+1] = (k >> 8) & 0xFF;
                }

                free (float_buffer); */
            }

            // store the values
            if (mSounds[i].mData) {
                //free(mSounds[i].mData); // seems this isnt needed ... the code sizes to compensate somehow
                mSounds[i].mData = NULL;
            }
            mSounds[i].mData = malloc(sizeof(char)*(length+1));
            for (j = 0; j < length; j=j+2) {
                mSounds[i].mData[j/2] = (short) ((buffer[j+1] << 8) | buffer[j]);
            }
            mSounds[i].mLength = (unsigned int) (length/2);
            mSounds[i].mPosition = 0;
            mSounds[i].mLoop = false;
            mSounds[i].mUsed = false;

            free(buffer);
            buffer = NULL;

        } else {
            // this midi note is not used -- disable it and clear data if it exists
            if (mSounds[i].mData) { free(mSounds[i].mData); mSounds[i].mData = NULL; }
            mSounds[i].mLength = 0;
            mSounds[i].mPosition = 0;
            mSounds[i].mLoop = false;
            mSounds[i].mUsed = false;
        }
    }

    // set the unique id
    mLoadedUID = uid;

    // Cleanup
    if (converted_input_samples) {
        av_freep(&converted_input_samples[0]);
        free(converted_input_samples);
    }
    (*env)->ReleaseIntArrayElements(env, freqIntArray, freqs, 0);
    return JNI_TRUE;
}

JNIEXPORT jint JNICALL
Java_com_jdmusiq_eartrainer_MySampler_loadedUID(JNIEnv *env, jclass type) {
    return (mLoadedUID);
}

JNIEXPORT void JNICALL
Java_com_jdmusiq_eartrainer_MySampler_playNote(JNIEnv *env, jclass type, int note) {
    if (note < 0 || note >= 128) {
        return;
    }

    mSounds[note].mPosition = 0;
    mSounds[note].mUsed = true;
}

JNIEXPORT void JNICALL
Java_com_jdmusiq_eartrainer_MySampler_stopNote(JNIEnv *env, jclass type, int note) {
    if (note < 0 || note >= 128) {
        return;
    }

    mSounds[note].mPosition = 0;
    mSounds[note].mUsed = false;
}

JNIEXPORT void JNICALL
Java_com_jdmusiq_eartrainer_MySampler_stopAllNotes(JNIEnv *env, jclass type) {
    for (int i = 0; i < AUDIO_OUT_CHANNELS; i++) {
        mSounds[i].mPosition = 0;
        mSounds[i].mUsed = 0;
    }

}

signed short convertAudioFloatToShort(float freq) {
    freq = freq * 32768; // SHRT_MAX
    if (freq > SHRT_MAX) freq = SHRT_MAX;
    if (freq < SHRT_MIN) freq = SHRT_MIN;

    return (short) freq;
}

float convertAudioShortToFloat(signed short freq) {
    float ret;

    ret = ((float) freq) / ((float) 32768); // SHRT_MAX
    if (ret > 1.0f) ret = 1.0f;
    if (ret < -1.0f) ret = -1.0f;

    return ret;
}

char* calculateMidiNoteName(int midinote) {
    int octave = (midinote / 12) - 1;
    int notenum = midinote % 12;

    char buffer[5]; // should not be longer than 4 let alone 5

    switch (notenum) {
        case 0: sprintf(buffer, "c%d", octave); break;
        case 1: sprintf(buffer, "c#%d", octave); break;
        case 2: sprintf(buffer, "d%d", octave); break;
        case 3: sprintf(buffer, "d#%d", octave); break;
        case 4: sprintf(buffer, "e%d", octave); break;
        case 5: sprintf(buffer, "f%d", octave); break;
        case 6: sprintf(buffer, "f#%d", octave); break;
        case 7: sprintf(buffer, "g%d", octave); break;
        case 8: sprintf(buffer, "g#%d", octave); break;
        case 9: sprintf(buffer, "a%d", octave); break;
        case 10: sprintf(buffer, "a#%d", octave); break;
        case 11: sprintf(buffer, "b%d", octave); break;
        default: sprintf(buffer, ""); break;
    }

    return buffer;
}

static int init_resampler(SwrContext **resample_context, int in_sample_rate, int out_sample_rate) {
    int error;
    // Create a resampler context for the conversion
    // Set the conversion parameters
    // Default channel layouts based on the number of channels
    // are assumed for simplicity (tey are sometimes not detected
    // properly by the demuxer and/or decoder
    *resample_context = swr_alloc_set_opts( NULL,
                                            AV_CH_LAYOUT_MONO, AV_SAMPLE_FMT_S16, out_sample_rate,
                                            AV_CH_LAYOUT_MONO, AV_SAMPLE_FMT_S16, in_sample_rate,
                                            0, NULL);

    if (!*resample_context) {
        LOGE("Could Not allocate resample context");
        return AVERROR(ENOMEM);
    }

    // open the resampler with the specified parameters.
    if ((error = swr_init(*resample_context)) < 0) {
        LOGE("Could not open resample context");
        swr_free(resample_context);
        return error;
    }

    LOGI("Resampling System Initialized");
    LOGI("Resampling: %dHz to %dHz", in_sample_rate, out_sample_rate);

    return 0;
}

// Initialize a temporary storage for the specified number of audio samples.
// The conversion requires temporary storage due to the different format.
// The number of audio samples to be allocated is specified in frame_size
static int init_converted_samples(uint8_t ***converted_input_samples, int frame_size) {
    int error;

    // Allocate as many pointers as there are audio channels.
    // Each pointer will later point to the audio samples of the corresponding
    // channels (although it may be NULL for interleaved formats).
    if (!(*converted_input_samples = calloc(1, sizeof(**converted_input_samples)))) {
        LOGE("Could not allocate converted input sample pointers");
        return AVERROR(ENOMEM);
    }

    // Allocate memory for the samples of all channels in one consecutive
    // block for convenience.
    if ((error = av_samples_alloc(*converted_input_samples, NULL,
                                  AUDIO_OUT_CHANNELS, frame_size, AV_SAMPLE_FMT_S16, 0 )) < 0) {
        LOGE("Could not allocate converted input samples");
        av_freep(&(*converted_input_samples)[0]);
        free(*converted_input_samples);
        return error;
    }

    return 0;
}

// Convert the input audio samples into the output sample format
// The conversion happens on a per-frame basis, the size of which is specified
// by frame_size
static int convert_samples(const uint8_t **input_data, uint8_t **converted_data, const int frame_size, SwrContext *resample_context) {
    int error;

    // Convert the samples using the resampler
    if ((error = swr_convert(resample_context, converted_data, frame_size, input_data, frame_size)) < 0) {
        LOGE("Could not convert input samples");
        return error;
    }
    converted_data = (uint8_t **) input_data;

    return 0;
}



// The player Callback
void soundPlayerCallback(SLAndroidSimpleBufferQueueItf aSoundQueue, void *aContext) {
    //LOGI("SOUND CALLBACK called");
    sendSoundBuffer();
}

void sendSoundBuffer() {
    SLuint32 result;
    prepareSoundBuffer();
    result=(*mSoundQueue)->Enqueue(mSoundQueue, mActiveAudioOutSoundBuffer,
                                   sizeof(signed short) * mFramesPerBuffer);
    if (result != SL_RESULT_SUCCESS)
    LOGE("enqueue method of sound buffer failed");

    swapSoundBuffer();
}

void prepareSoundBuffer() {
    signed int *tmp = mTmpSoundBuffer;
    signed int i, j;
    int processed, toprocess;

    memset(mTmpSoundBuffer, 0, sizeof(signed int) * mFramesPerBuffer);

    for (i = 0; i < AUDIO_OUT_CHANNELS; i++) {
        SoundInfo * sound = &mSounds[i];
        if (sound->mUsed) {

            processed = 0;
            toprocess = 0;
            while (processed < mFramesPerBuffer) {
                signed int addLength = min(mFramesPerBuffer-processed, sound->mLength - sound->mPosition);
                signed short *addData = sound->mData + sound->mPosition;

                if (sound->mPosition + addLength >= sound->mLength) {
                    // the sound is out of samples to play
                    // check if we should loop it
                    if (sound->mLoop) {
                        // YES: Loop
                        toprocess += addLength;
                        sound->mPosition = 0;
                    } else {
                        sound->mUsed = false;
                        toprocess += mFramesPerBuffer; // causes the script to exit on the next round
                    }
                } else {
                    sound->mPosition += addLength;
                    toprocess += addLength;
                }

                for(j = 0; j < addLength; j++) {
                    if (addData) tmp[j+processed] += addData[j];
                }

                processed += toprocess;
            }
        }
    }

    // finalize(clip) output buffer
    signed short* dataOut = mActiveAudioOutSoundBuffer;
    for (j = 0; j < mFramesPerBuffer; j++) {
        if (tmp[j] > SHRT_MAX)
            dataOut[j] = SHRT_MAX;
        else if (tmp[j] < SHRT_MIN)
            dataOut[j] = SHRT_MIN;
        else
            dataOut[j] = (short) tmp[j];
    }
}

void swapSoundBuffer() {
    if (mActiveAudioOutSoundBuffer == mAudioOutSoundData1) {
        mActiveAudioOutSoundBuffer = mAudioOutSoundData2;
    } else {
        mActiveAudioOutSoundBuffer = mAudioOutSoundData1;
    }
}