/**
 * mySampler.h - This File includes all of the NDK header code for the
 * Java Class: MySampler, including it's private functions & subroutines.
 * The functionality includes, running OpenSLES implementation for the sound
 * engine as well as a sample based synthesizer that re-samples (stretch-tunes)
 * notes to another note to ensure all notes are covered with less data.
 *
 * @author Jason Davis
 * @version 1.0
 */

#ifndef EARTRAINER_MYSAMPLER_H
#define EARTRAINER_MYSAMPLER_H

#include <assert.h>
#include <jni.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>

// external libraries
#include "libavutil/avutil.h"
#include "libswresample/swresample.h"

// OpenSLES - native audio
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

// native asset manager
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

// log system
#include <android/log.h>
#include <ffmpeg/include/libswresample/swresample.h>

#define TAG "eartrainer"
#if NDEBUG
    #define LOGV(...)
    #define LOGE(...)
    #define LOGI(...)
#else
    #define LOGV(...) {__android_log_print(ANDROID_LOG_VERBOSE, TAG, __VA_ARGS__);}
    #define LOGE(...) {__android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__);}
    #define LOGI(...) {__android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__);}
#endif

// CONSTANTS
#define AUDIO_OUT_CHANNELS 128 // Different Midi Notes
#define DEFAULT_SAMPLE_RATE 44100
#define DEFAULT_FRAMES_PER_BUFFER 512
#define WAVE_FILE_HEADER_SIZE 46

// Max and Min Functions
#define max(a,b) \
    ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
       _a > _b ? _a : _ b; })
#define min(a,b) \
    ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
       _a < _b ? _a : _b; })

// Global Variables
uint8_t *mTmpData = NULL;
signed short *mTmpData2 = NULL;
long tmpDataCount;

// Sound Info Structure
typedef struct sSoundInfo {
    // Sound ID
    unsigned short mSoundID;

    // used & Priority
    bool mUsed; // >1 = true
    signed short* mData;

    // Length and position and where it started
    unsigned int mLength;
    unsigned int mPosition;
    unsigned int mStarted;

    // Loop it
    bool mLoop;
} SoundInfo;

SoundInfo mSounds[AUDIO_OUT_CHANNELS];

// Sample Rate storage and Frames per Buffer
int mSampleRate;
int mFramesPerBuffer;
int mLoadedUID = -1;

// engine interfaces
static SLObjectItf mEngineObj = NULL;
static SLEngineItf mEngine = NULL;

// Output mix interfaces
static SLObjectItf mOutputMixObj = NULL;

// Sound Objects
static SLObjectItf mSoundPlayerObj = NULL;
static SLPlayItf mSoundPlayer = NULL;
static SLVolumeItf mSoundVolume = NULL;
static SLAndroidSimpleBufferQueueItf mSoundQueue = NULL;

// Audio Buffers
signed int *mTmpSoundBuffer;
signed short *mAudioOutSoundData1;
signed short *mAudioOutSoundData2;
signed short *mActiveAudioOutSoundBuffer;

// Exported Functions
JNIEXPORT jstring JNICALL Java_com_jdmusiq_eartrainer_MySampler_testjni(JNIEnv *env, jobject instance);
JNIEXPORT void JNICALL Java_com_jdmusiq_eartrainer_MySampler_startEngine(JNIEnv *env, jclass type);
JNIEXPORT void JNICALL Java_com_jdmusiq_eartrainer_MySampler_stopEngine(JNIEnv *env, jobject instance);
JNIEXPORT void JNICALL Java_com_jdmusiq_eartrainer_MySampler_startSoundPlayer(JNIEnv *env, jclass type, jint sampleRate, jint framesPerBuffer);
JNIEXPORT void JNICALL Java_com_jdmusiq_eartrainer_MySampler_stopSoundPlayer(JNIEnv *env, jclass type);
JNIEXPORT jboolean JNICALL Java_com_jdmusiq_eartrainer_MySampler_isEngineStarted(JNIEnv *env, jclass type);
JNIEXPORT jboolean JNICALL Java_com_jdmusiq_eartrainer_MySampler_isSoundPlayerStarted(JNIEnv *env, jclass type);
JNIEXPORT jboolean JNICALL Java_com_jdmusiq_eartrainer_MySampler_isPlaying(JNIEnv *env, jclass type);
JNIEXPORT jboolean JNICALL Java_com_jdmusiq_eartrainer_MySampler_loadMidiSamples(JNIEnv *env, jclass type, jint uid, jobject assetManager, jobjectArray pathStringArray, jintArray freqIntArray);
JNIEXPORT jint JNICALL Java_com_jdmusiq_eartrainer_MySampler_loadedUID(JNIEnv *env, jclass type);
JNIEXPORT void JNICALL Java_com_jdmusiq_eartrainer_MySampler_playNote(JNIEnv *env, jclass type, int note);
JNIEXPORT void JNICALL Java_com_jdmusiq_eartrainer_MySampler_stopNote(JNIEnv *env, jclass type, int note);
JNIEXPORT void JNICALL Java_com_jdmusiq_eartrainer_MySampler_stopAllNotes(JNIEnv *env, jclass type);

// internal functions
signed short convertAudioFloatToShort(float freq);
float convertAudioShortToFloat(signed short freq);
char* calculateMidiNoteName(int midinote);
static int init_resampler(SwrContext **resample_context, int in_sample_rate, int out_sample_rate);
static int init_converted_samples(uint8_t ***converted_input_samples, int frame_size);
static int convert_samples(const uint8_t **input_data, uint8_t **converted_data, const int frame_size, SwrContext *resample_context);

void soundPlayerCallback(SLAndroidSimpleBufferQueueItf aSoundQueue, void *aContext);
void sendSoundBuffer();
void prepareSoundBuffer();
void swapSoundBuffer();

#endif //EARTRAINER_MYSAMPLER_H
